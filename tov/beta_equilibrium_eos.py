import numpy as np 
import h5py as h5 
import math

rho_gf = 1.61930347e-18 
press_gf = 1.80171810e-39
eps_gf = 1.11265006e-21 

def get_equilibrium_at_T_rho(T, rho, ye, lT, lpress, lenergy, munu, lrho, entropy):

  ridx = 0
  for i in range(len(lrho)): 
    if rho<10.0**lrho[i]: 
      ridx = i-1
      break 
  tidx = 0   
  for i in range(len(lT)): 
    if T<10.0**lT[i]: 
      tidx = i-1
      break
      
  ft = (T - 10.0**lT[tidx])/(10.0**lT[tidx+1]-10.0**lT[tidx])
  fr = (rho - 10.0**lrho[ridx])/(10.0**lrho[ridx+1]-10.0**lrho[ridx])
  if (ft > 1.0): ft = 1.0
  if (ft < 0.0): ft = 0.0  
  if (fr > 1.0): ft = 1.0
  if (fr < 0.0): ft = 0.0  
  
  def find_beta_equil_ye_at_point(iT, irho):
    zero_cross = np.where(np.diff(np.sign(munu[:,iT,irho])))[0]
    j = iT
    i = irho 
    if len(zero_cross)<1 and munu[0,j,i]<0.0:
      zero_cross = len(ye)-1
    elif len(zero_cross)<1 and munu[0,j,i]>0.0:
      zero_cross = 0
    else: 
      zero_cross = zero_cross[0]
    s = 0; lp = 0; yeeq = 0; le = 0
    if zero_cross < len(ye)-1:
      f = -( munu[zero_cross,j,i] / (munu[zero_cross+1,j,i] - munu[zero_cross,j,i]))
      if f<0.0: f=0.0 
      if f>1.0: f=1.0
      s = entropy[zero_cross+1,j,i]*f + entropy[zero_cross,j,i]*(1.0-f)     
      yeeq = ye[zero_cross+1]*f + ye[zero_cross]*(1.0-f)  
      lp = (lpress[zero_cross,j,i]*(1.0 - f) + lpress[zero_cross+1,j,i]*f)
      le = (lenergy[zero_cross,j,i]*(1.0 - f) + lenergy[zero_cross+1,j,i]*f) 
    else:  
      s = entropy[-1,j,i]
      yeeq = ye[-1]
      lp = lpress[-1,j,i]
      le = lenergy[-1,j,i]
    return (s, yeeq, lp, le)
   
  s = 0; yeq = 0; lp = 0; le = 0; 
  if ft<1.0-1.e-10 and fr<1.0-1.e-10: 
    vll = find_beta_equil_ye_at_point(tidx, ridx)
    s  += (1-ft)*(1-fr)*vll[0]
    yeq += (1-ft)*(1-fr)*vll[1]
    lp += (1-ft)*(1-fr)*vll[2]
    le += (1-ft)*(1-fr)*vll[3]
  
  if ft>1.e-10 and fr<1.0-1.e-10: 
    vll = find_beta_equil_ye_at_point(tidx+1, ridx)
    s  += ft*(1-fr)*vll[0]
    yeq += ft*(1-fr)*vll[1]
    lp += ft*(1-fr)*vll[2]
    le += ft*(1-fr)*vll[3]
  
  if fr>1.e-10 and ft<1.0-1.e-10: 
    vll = find_beta_equil_ye_at_point(tidx, ridx+1)
    s  += (1-ft)*fr*vll[0]
    yeq += (1-ft)*fr*vll[1]
    lp += (1-ft)*fr*vll[2]
    le += (1-ft)*fr*vll[3]
  
  if fr>1.e-10 and ft>1.e-10: 
    vll = find_beta_equil_ye_at_point(tidx+1, ridx+1)
    s  += ft*fr*vll[0]
    yeq += ft*fr*vll[1]
    lp += ft*fr*vll[2]
    le += ft*fr*vll[3]
  return (yeq, s, lp, le) 


class beta_equilibrium_eos: 
  """ Class for holding a one dimensional equation of state.
  """   
  def __init__(self, lrho=[], lpress=[], lenergy=[], ye=[], energy_offset=0.0, Tbeta=0.009):  
    self.lrho = lrho 
    self.lpress_bequil  = lpress
    self.lenergy_bequil = lenergy
    self.ye_bequil      = ye
    self.energy_offset = energy_offset
    self.Tbeta = Tbeta
     
  @classmethod  
  def build_from_stellarcollapse(cls, fname, Tbeta=0.009): 
    # Open the hdf file and read it in
    ff = h5.File(fname) 
    ye = ff['/ye'] 
    lT = ff['/logtemp'] 

    lpress = ff['/logpress'] 
    lenergy = ff['/logenergy'] 
    munu = ff['/munu'] 
    
    energy_offset = ff['/energy_shift'][0]
    
    # Initialize the class variables 
    lrho = ff['/logrho'] 
    lpress_bequil  = np.zeros((len(lrho), )) 
    lenergy_bequil = np.zeros((len(lrho), ))
    ye_bequil      = np.zeros((len(lrho), ))
    
    for (i, ltc) in enumerate(lT):
      T = 10.0**ltc 
      if (T>Tbeta): 
        iT = i
        break
     
    for i in range(len(lrho)): 
      #for j in range(len(lT)):
      for j in range(1):
        j = iT 
        zero_cross = np.where(np.diff(np.sign(munu[:,j,i])))[0]
        
        if len(zero_cross)<1 and munu[0,j,i]<0.0:
          zero_cross = len(ye)-1
        elif len(zero_cross)<1 and munu[0,j,i]>0.0:
          zero_cross = 0
        else: 
          zero_cross = zero_cross[0]   
        
        if zero_cross < len(ye)-1:
          f = -( munu[zero_cross,j,i] 
            / (munu[zero_cross+1,j,i] - munu[zero_cross,j,i]))
          
          if f<0.0: f=0.0 
          if f>1.0: f=1.0
             
          lpress_bequil[i] = (lpress[zero_cross,j,i]*(1.0 - f)
            + lpress[zero_cross+1,j,i]*f)
          lenergy_bequil[i] = (lenergy[zero_cross,j,i]*(1.0 - f)  
            + lenergy[zero_cross+1,j,i]*f) 
          ye_bequil[i] = (ye[zero_cross]*(1.0 - f)  
            + ye[zero_cross+1]*f)
          #print math.pow(10, lrho[i]), math.pow(10, lenergy_bequil[i]), math.pow(10, lpress_bequil[i]), ye_bequil[i],
          if lpress_bequil[i] < lpress_bequil[i-1]:
            lpress_bequil[i] = lpress_bequil[i-1]*(1.0 + 1.e-10)
          #print math.pow(10, lpress_bequil[i]) 
        else:  
          lpress_bequil[i] = lpress[-1,j,i]  
          lenergy_bequil[i] = lenergy[-1,j,i]   
          ye_bequil[i] = ye[-1] 
    return cls(lrho, lpress_bequil, lenergy_bequil, ye_bequil, energy_offset, 10.0**lT[iT]) 
  

  @classmethod  
  def build_from_stellarcollapse_rhovsT(cls, fname, rho, T): 
    # Open the hdf file and read it in
    ff = h5.File(fname) 
    ye = ff['/ye'] 
    lT = ff['/logtemp'] 

    entropy = ff['/entropy']
    lpress = ff['/logpress'] 
    lenergy = ff['/logenergy'] 
    munu = ff['/munu'] 
    
    energy_offset = ff['/energy_shift'][0]
    
    # Initialize the class variables 
    lrho = ff['/logrho'] 
    lpress_bequil  = np.zeros((len(rho), )) 
    lenergy_bequil = np.zeros((len(rho), ))
    ye_bequil      = np.zeros((len(rho), ))
    
    for (i, r) in enumerate(rho):
      (yeeq, s, lp, le) = get_equilibrium_at_T_rho(T[i], r, ye, lT, lpress, lenergy, munu, lrho, entropy)
      lpress_bequil[i] = lp
      lenergy_bequil[i] = le
      ye_bequil[i] = yeeq

    return cls(np.log10(rho), lpress_bequil, lenergy_bequil, ye_bequil, energy_offset, T) 
      
  @classmethod  
  def build_from_stellarcollapse_entropy(cls, fname, scons=8.0): 
    
    # Open the hdf file and read it in
    ff = h5.File(fname) 
    ye = ff['/ye'] 
    lT = ff['/logtemp'] 
    
    entropy = ff['/entropy']
    lpress = ff['/logpress'] 
    lenergy = ff['/logenergy'] 
    munu = ff['/munu'] 
    
    energy_offset = ff['/energy_shift'][0]
    
    # Initialize the class variables 
    lrho = ff['/logrho'] 
    lpress_bequil  = np.zeros((len(lrho), )) 
    lenergy_bequil = np.zeros((len(lrho), ))
    ye_bequil      = np.zeros((len(lrho), ))
    T_bequil      = np.zeros((len(lrho), ))
    
    for i in range(len(lrho)): 
      if 10.0**lrho[i] < 1.e7 or 10.0**lrho[i]>1.e13:
        continue 
     
      yeeq_last = -1.0 
      yeeq = -1.0
      s = -1.0
      slast = -1.0
      le = -1.0 
      lelast = -1.0 
      lp = -1.0 
      lplast = -1.0
      T = -1.0
      Tlast = -1.0
      for j in range(len(lT)): 
        zero_cross = np.where(np.diff(np.sign(munu[:,j,i])))[0]
        
        if len(zero_cross)<1 and munu[0,j,i]<0.0:
          zero_cross = len(ye)-1
        elif len(zero_cross)<1 and munu[0,j,i]>0.0:
          zero_cross = 0
        else: 
          zero_cross = zero_cross[0]

        T = 10.0**lT[j]
        if zero_cross < len(ye)-1:
          f = -( munu[zero_cross,j,i] / (munu[zero_cross+1,j,i] - munu[zero_cross,j,i]))
          if f<0.0: f=0.0 
          if f>1.0: f=1.0
          s = entropy[zero_cross+1,j,i]*f + entropy[zero_cross,j,i]*(1.0-f)     
          yeeq = ye[zero_cross+1]*f + ye[zero_cross]*(1.0-f)  
          lp = (lpress[zero_cross,j,i]*(1.0 - f) + lpress[zero_cross+1,j,i]*f)
          le = (lenergy[zero_cross,j,i]*(1.0 - f) + lenergy[zero_cross+1,j,i]*f) 
        else:  
          s = entropy[-1,j,i]
          yeeq = ye[-1]
          lp = lpress[-1,j,i]
          le = lenergy[-1,j,i]
        if s>scons:
          break 
        yeeq_last = yeeq
        slast = s
        lplast = lp
        lelast = le  
        Tlast = T

      f = (scons - slast)/(s - slast)
      if slast < 0.0: 
        f = 1.0 
      ye_bequil[i] = yeeq*f +  yeeq_last*(1-f)
      lpress_bequil[i] = lp*f +  lplast*(1-f)
      lenergy_bequil[i] = le*f +  lelast*(1-f)
      T_bequil[i] = T*f + Tlast*(1-f) 
       
    return cls(lrho, lpress_bequil, lenergy_bequil, ye_bequil, energy_offset, T_bequil) 
  
  def get_beta_equilibrium_state_from_p(self, p):
    lp = math.log10(p) 
    idx = np.digitize([lp],self.lpress_bequil[:]) 
    idx = idx[0] 
    if idx >= len(self.lpress_bequil): idx = len(self.lpress_bequil) - 1  
    if idx < 1: idx = 1
    f = ((lp - self.lpress_bequil[idx-1]) 
        / (self.lpress_bequil[idx] - self.lpress_bequil[idx-1]))
    if f<0.0: f = 0.0 
    if f>1.0: f = 1.0
    lenerg = self.lenergy_bequil[idx]*f + self.lenergy_bequil[idx-1]*(1-f)
    lrho = self.lrho[idx]*f + self.lrho[idx-1]*(1-f)
    ye = self.ye_bequil[idx]*f + self.ye_bequil[idx-1]*(1-f)
    
    return (math.pow(10,lrho), math.pow(10,lenerg) - self.energy_offset, ye) 
  
  def get_beta_equilibrium_state_from_rho(self, rho):
    lrho = math.log10(rho) 
    idx = np.digitize([lrho],self.lrho) 
    idx = idx[0]
    if idx >= len(self.lrho): idx = len(self.lrho) - 1  
    if idx < 1: idx = 1
    f = ((lrho - self.lrho[idx-1]) 
        / (self.lrho[idx] - self.lrho[idx-1]))
    if f<0.0: f = 0.0 
    if f>1.0: f = 1.0
    lenerg = self.lenergy_bequil[idx]*f + self.lenergy_bequil[idx-1]*(1-f)
    lpress = self.lpress_bequil[idx]*f + self.lpress_bequil[idx-1]*(1-f)
    ye = self.ye_bequil[idx]*f + self.ye_bequil[idx-1]*(1-f)
    
    return (math.pow(10,lpress), math.pow(10,lenerg) - self.energy_offset, ye) 

  def write_beta_equilibrium_ye(self, fname='eos_out'):
    f = open(fname,'w')
    #f.write("# Temperature: {0:e} \n".format(float(self.Tbeta)))
    f.write("# Energy Offset: {0:e} \n".format(float(self.energy_offset))) 
    f.write("# [1] rho \n") 
    f.write("# [2] logp \n") 
    f.write("# [3] loge \n") 
    f.write("# [4] y_e,beta \n") 
    f.write("# [5] T,beta \n") 
    for (i, lrho) in enumerate(self.lrho): 
      #if len(self.Tbeta)>1: 
      #  Tb = self.Tbeta[i]
      #else:
      #  Tb = self.Tbeta
      Tb = self.Tbeta
      f.write("{0:e} {1:e} {2:e} {3:e} {4:e}\n".format(10.0**lrho, 
          #self.lpress_bequil[i] + math.log10(press_gf), 
          #self.lenergy_bequil[i] + math.log10(eps_gf), 
          self.lpress_bequil[i],
          self.lenergy_bequil[i], 
          self.ye_bequil[i], Tb))

