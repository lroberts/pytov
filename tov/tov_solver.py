import math
import numpy as np  
from tov.beta_equilibrium_eos import beta_equilibrium_eos as beq_eos
from tov.skyrme_eos import skyrme_eos
import scipy.integrate as integrate
import scipy.optimize as optimize 
 
L0 = 1.477e5    # one msun in cm 
T0 = 4.92673e-6 # one msun in seconds
M0 = 1.98892e33 # one msun in g 
mbaryon = 1.6605402e-24 
Eden0 = M0/(L0*T0**2)
Rho0 = M0/L0**3
N0 = M0/mbaryon 

class tov_model: 
  """ Stores the structure of a TOV star.

  """
  def __init__(self, rad, gmass, bmass, lapse, rho, ye, press, eden):
    """ Load the variables defining a NSs structure.  
    """
    self.rad = rad
    self.gmass = gmass
    self.bmass = bmass
    self.lapse = lapse 
    self.rho = rho 
    self.ye = ye 
    self.press = press 
    self.eden = eden 
  
  def get_total_bmass(self):
    """ Return the total baryonic mass of the NS in solar masses.
    """
    return self.bmass[-1] 
  
  def get_total_gmass(self):
    """ Return the total gravitational mass of the NS in solar masses.
    """
    return self.gmass[-1]
  
  def get_radius(self):
    """ Return the radius of the NS in cm.
    """
    return self.rad[-1]
  
  def get_ye_bins(self, nbins=50, bin_range=(0.0, 0.3), mass_in=-1.0): 
    """ Return electron fractions with zone baryonic masses
    """ 
    da = 0.5*(self.bmass[2:] - self.bmass[0:-2])
    da = np.insert(da, 0, 0.5*self.bmass[2]) 
    da = np.insert(da, len(da), 0.5*(self.bmass[-1] - self.bmass[-2]))
    
    
    idx = 0
    if mass_in>0.0:
      # Only return the outer most mass_in distribution
      mass = 0.0 
      for i in reversed(range(len(self.bmass))):
        if (self.bmass[-1] - self.bmass[i]) > mass_in:
          idx = i 
          break 
    return self.ye[idx:], da[idx:] 
       
  def print_model(self): 
    """ Output the model to screen. (need to update to allow for file output).
    """
    for (i, r) in enumerate(self.rad): 
      print ("{0:e} {1:e} {2:e} {3:e} {4:e} {5:e} {6:e} {7:e}".format(r, 
          self.gmass[i], self.bmass[i], self.lapse[i], self.rho[i], 
          self.eden[i], self.press[i], self.ye[i]))
     
class tov_solver:
  """ Numerically integrates the TOV equations for a given EoS.  

  """
  def __init__(self, eos):
    """ Initialize the chosen EoS.
    """ 
    self.eos = eos 

  def tovrhs(self, y, lp):
    """ Return the right hand sides of the TOV equations. 
    
    RHSs of the radius, lapse, pressure, and gravitational mass TOV ODEs with 
    respect to the baryonic mass are calculated, then transformed to be 
    derivatives with respect to the pressure.  This mean the last RHS is the 
    RHS of the enclosed baryonic mass ODE.  
    """ 
    r   = y[0] 
    m   = y[1]
    alp = y[2] 
    a   = y[3]
    
    # Calculate some required quantities
    gam = math.sqrt(1.0 - 2.0*m/r)
    rho, eps, ye = self.eos.get_beta_equilibrium_state_from_p(math.exp(lp)*Eden0) 
    nb  = rho/mbaryon*L0**3/N0 
    p   = math.exp(lp)  
    E   = (rho * eps)/Eden0 + rho/Rho0
    rho = rho/Rho0
      
    # Write the equations in terms of baryonic mass derivatives
    dalpda = alp*(m + 4.0*math.pi*r**3*p)/(4.0*math.pi*r**4*nb*gam)
    dpda = -(E + p)/alp*dalpda 
    dmda = gam*E/nb 
    drda = gam/(4.0*math.pi*r**2*nb)
     
    return [p*drda/dpda, p*dmda/dpda, p*dalpda/dpda, p/dpda]   

  def solve_tov(self, rhostart, rhoend, n_grid_points=300):
    """ Integrate the TOV ODEs to find the structure of a NS with given central density. 

    rhostart = The central density of the NS 
    rhoend = density at which to stop integration 
    n_grid_points = number of grid points to calculate structure at,
                    grid points are chosen to be logarithmic in pressure 
    """ 
    pstart, estart, ye = self.eos.get_beta_equilibrium_state_from_rho(rhostart) 
    pend, eend, ye = self.eos.get_beta_equilibrium_state_from_rho(rhoend) 
  
    rstart = 1.e-5
    astart = 4.0*math.pi*rstart**3*rhostart/mbaryon*L0**3/3.0/N0
    mstart = 4.0*math.pi*rstart**3*(estart*rhostart/Eden0 + rhostart/Rho0) 
    alpstart = 1
    
    lpgrid = np.arange(math.log(pstart/Eden0), math.log(pend/Eden0), 
        (math.log(pend/Eden0) - math.log(pstart/Eden0))/n_grid_points)  
  
    y = integrate.odeint(self.tovrhs, [rstart, mstart, alpstart, astart], lpgrid)
    rad   = y[:,0]*L0 
    gmass = y[:,1] 
    bmass = y[:,3] 
    lapse = y[:,2] - y[-1,2] + 1  
    ye = np.zeros(lpgrid.shape)   
    rho = np.zeros(lpgrid.shape)   
    press = np.zeros(lpgrid.shape)   
    eden = np.zeros(lpgrid.shape)   
    for i in range(len(ye)): 
      press[i] = math.exp(lpgrid[i])*Eden0 
      rho[i], eden[i], ye[i] = self.eos.get_beta_equilibrium_state_from_p(press[i]) 
    return tov_model(rad, gmass, bmass, lapse, rho, ye, press, eden)

  def find_mass(self, mass0, rhomin=1e14, rhomax=7e15, rhoend=1.e9, 
      get_gmass=True, n_grid_points=300):
    """ Find a TOV solution for a star of a given mass. 
    
      mass0 = mass of star 
      rhomin = minimum central density to start at for bracketing 
      rhomax = maximum central density to start at for bracketing 
      rhoend = density at which to end TOV integration 
      get_gmass = if true, search for gravitational mass, if false 
                  search for baryonic mass 
    """ 

    def get_mass(rhoc):
      mod = self.solve_tov(rhoc, rhoend, 10)
      if (get_gmass): 
        return mod.get_total_gmass() - mass0
      mod.get_total_bmass() - mass0 
    
    rho_sol = optimize.bisect(get_mass, rhomin, rhomax, rtol=1.e-4)  
    
    return self.solve_tov(rho_sol, rhoend, n_grid_points=n_grid_points) 

