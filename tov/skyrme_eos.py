import numpy as np 
import h5py as h5 
import math
from tov.beta_equilibrium_eos import beta_equilibrium_eos

HBC = 197.3
MNUC = 938.918/HBC
class skyrme_eos: 
  """ Class that creates Skyrme EOS from parameter set 
  """
  def __init__(self, A=-711.002, B=-107.06, C=934.62, D=0.0, 
      F=0.0, G=0.0, delta=1.2602852, gamma=-1.0, nsat=0.166, Kb=220.0): 
    self.A = A/HBC
    self.B = B/HBC 
    self.C = C/HBC 
    self.D = D/HBC 
    self.F = F/HBC 
    self.G = G/HBC
    self.delta = delta
   
     
    self.H = 0.0 
    self.gamma = 1.0
    
    # Currently we are using arbitrary values for the saturation density and 
    # compressibility 
    if (gamma > 0.0): 
      self.H = Kb*0.01/HBC/((gamma-1.0)*gamma*nsat**(gamma-1.0))
      self.gamma = gamma 
    

  @classmethod 
  def build_from_skyrme_params(cls, t0=-2484.97, t1=266.735, t2=-337.135, 
      t3=14588.2, x0=0.06277, x1=0.65845, x2=-0.95382, x3=-0.03413, 
      sigma=0.1667, gamma=-1.0, nsat=0.1746, Kb=210.0):
    A = t0/4.*(1.-x0)    # [MeV*fm^3]
    B = t0/8.*(1.+2.*x0) # [MeV*fm^3]
    C = t3/24.*(1.-x3)    # [MeV*fm^3]
    D = t3/48.*(1.+2.*x3) # [MeV*fm^3]
    delta  = 1.+sigma # Dimensionless
    
    alpha1 = 1./8.*(t1*(1.-x1)+3.*t2*(1.+x2)) # [MeV*fm^5]
    alpha2 = 1./8.*(t1*(2.+x1)+1.*t2*(2.+x2)) # [MeV*fm^5]
    F = 0.5*(alpha2 + alpha1)    
    G = 0.5*(alpha2 - alpha1)    

    return cls(A, B, C, D, F, G, delta, gamma, nsat, Kb) 
     
  def get_beta_equil_eos(self):
    lrho = [] 
    lpress = []
    lenergy = [] 
    ye = [] 
    energy_offset = 0.0 
    for lr in np.arange(16.0, 2.9, -0.1):
      rho = 10.0**lr 
      yec = self.get_beta_equil_ye(rho) 
      (ee, pp, munu) = self.get_state(rho, yec)
      if (len(lpress)>0):
        if (math.log10(pp) >= lpress[-1]):
          continue 
      
      lrho.append(lr) 
      lpress.append(math.log10(pp)) 
      lenergy.append(math.log10(ee - energy_offset)) 
      ye.append(yec)
    
    lrho.reverse()
    lpress.reverse()
    lenergy.reverse()
    ye.reverse() 
     
    return beta_equilibrium_eos(lrho, lpress, lenergy, ye, energy_offset) 

      
  def get_beta_equil_ye(self, rho): 
    ye_lo = 0.0 
    ye_up = 0.6
    (t1, t2, mu_lo) = self.get_state(rho, ye_lo)   
    (t1, t2, mu_up) = self.get_state(rho, ye_up)   
    for i in range(30): 
      ye_mid = 0.5*(ye_lo + ye_up) 
      (t1, t2, mu_mid) = self.get_state(rho, ye_mid)   
      if (mu_mid*mu_lo>0.0): 
        mu_lo = mu_mid 
        ye_lo = ye_mid 
      else:
        mu_up = mu_mid 
        ye_up = ye_mid 

    return 0.5*(ye_lo + ye_up)  
    
     
  def get_state(self, rho, xp):
    
    nt = rho/1.674e15 
    MNUC = 938.918/HBC
    np = nt*xp 
    nn = nt*(1.0 - xp) 
    
    mA = self.A 
    mB = self.B
    mC = self.C
    mD = self.D
    mF = self.F 
    mG = self.G 
    mH = self.H

    mDelta = self.delta 
    mGamma = self.gamma 
     
    PI = math.pi 

    momsp = 1.0 + mF*(nn + np) + mG*(nn - np)  
    momsn = 1.0 + mF*(nn + np) - mG*(nn - np)  
    
    etap = (3.0*PI**2*np)**(2.0/3.0)/(2.0*MNUC/momsp) 
    etan = (3.0*PI**2*nn)**(2.0/3.0)/(2.0*MNUC/momsn) 
    
    taup = (3.0*PI**2*np)**(5.0/3.0)/(5.0*PI**2)
    taun = (3.0*PI**2*nn)**(5.0/3.0)/(5.0*PI**2)

    Up = ((taup*(mF - mG) + taun*(mF + mG)) / (2.0*MNUC) 
        + 2.0*mA*nt + 4.0*mB*nn + mC*(1.0 + mDelta)*nt**mDelta 
        + 4.0*mD*nt**(mDelta-2.0)*(nn*nt + (mDelta-1.0)*nn*np)
        + mGamma*mH*nt**(mGamma-1.0));
    Un = ((taup*(mF + mG) + taun*(mF - mG)) / (2.0*MNUC) 
        + 2.0*mA*nt + 4.0*mB*np + mC*(1.0 + mDelta)*nt**mDelta
        + 4.0*mD*nt**(mDelta-2.0)*(np*nt + (mDelta-1.0)*nn*np)
        + mGamma*mH*nt**(mGamma-1.0));
         
    mup = etap + Up; 
    mun = etan + Un; 
  
    ee = (taup*momsp/(2.0*MNUC) + taun*momsn/(2.0*MNUC) 
      + (mA + 4.0*mB*xp*(1.0-xp))*nt*nt 
      + (mC + 4.0*mD*xp*(1.0-xp))*nt**(mDelta + 1.0)
      + mH*nt**mGamma);
    
    pp = ((5.0/6.0*momsp - 0.5)/MNUC*taup + (5.0/6.0*momsn - 0.5)/MNUC*taun
      + (mA + 4.0*mB*xp*(1.0-xp))*nt*nt 
      + (mC + 4.0*mD*xp*(1.0-xp))*mDelta*nt**(mDelta + 1.0)
      + (mGamma-1.0)*mH*nt**mGamma);

    # Add in the relativistic degenerate electron contribution
    mue = (3.0*PI**2*xp*nt)**(1.0/3.0) 
    eel = mue**4/(4.0*PI**2) 
    pel = eel/3.0
    
    ee = ((ee + eel)*HBC)*1.6021772e33/rho
    pp = ((pp + pel)*HBC)*1.6021772e33
    munu = (mup - mun + mue)*HBC

    return (ee, pp, munu) 

#sk = skyrme_eos()
#
#beta_eos = sk.get_beta_equil_eos()
#for lnb in np.arange(3, 16.0, 0.1):
#  nb = 10.0**lnb
#  ye = sk.get_beta_equil_ye(nb)
#  (ee, pp, munu) = sk.get_state(nb, ye)
#  print nb, ye, ee, pp, munu
