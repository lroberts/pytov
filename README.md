# README #

This package provides classes for solving the TOV equations for a given P(rho). It also provides routines for creating such equations of state from Skyrme parameterizations and stellarcollapse.org style EoS tables. 

### How do I get set up? ###

* Requires numpy, scipy, and h5py 
* TO INSTALL: run 'python setup.py install' in the base directory 
* See Mass_radius.py for an example of usage. 

### Who do I talk to? ###

* For questions, contact Luke Roberts (luke.roberts.36@gmail.com) 
