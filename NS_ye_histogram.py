import numpy as np  
import matplotlib.pyplot as plt 
import tov.tov_solver as tov_solver   
import tov.beta_equilibrium_eos as beq_eos

eos = beq_eos.beta_equilibrium_eos.build_from_stellarcollapse('./LS220_234r_136t_50y_analmu_20091212_SVNr26.h5')
tov_sol = tov_solver.tov_solver(eos)
rhocg = np.arange(14, 16, 0.01) 
rhocg = np.power(10, rhocg)

mod = tov_sol.find_mass(1.4, n_grid_points=3000)
#mod.print_model() 
ye, da = mod.get_ye_bins(mass_in=0.1)
print ye 
plt.hist(ye, weights=da, bins=100, range=(0.0, 0.5), histtype='stepfilled') 
plt.show()
