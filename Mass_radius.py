import sys 
import numpy as np  
import matplotlib.pyplot as plt 
import tov.tov_solver as tov_solver   
import tov.beta_equilibrium_eos as beq_eos
import tov.skyrme_eos as skyrme_eos

gamma = -1.0
if len(sys.argv)>1:
  gamma = float(sys.argv[1]) 

# Build Skyrme eos from (almost) LS style parameters plus gamma index 
# (F = (alpha1 + alpha2)/2 and G = (alpha2-alpha1)/2
ls = skyrme_eos.skyrme_eos(A=-711.002, B=-107.06, C=934.62, D=0.0, F=0.0, G=0.0, 
    delta=1.2602852,gamma=gamma)

# Build Skyrme eos from standard skyrme style parameters plus gamma index 
sk = skyrme_eos.skyrme_eos.build_from_skyrme_params(t0=-2484.97, t1=266.735, 
    t2=-337.135, t3=14588.2, x0=0.06277, x1=0.65845, x2=-0.95382, x3=-0.03413, 
    sigma=0.1667,gamma=gamma) 

eos = sk.get_beta_equil_eos() 

#eos = beq_eos.beta_equilibrium_eos.build_from_stellarcollapse("/home/lroberts/data/hera/andre_tables/LSEOSF90_LS220_rho391_temp133_ye65_version1.0_20160225.h5")

#eos = beq_eos.beta_equilibrium_eos.build_from_stellarcollapse("./LS220_0000_rho395_temp163_ye66_version1.0_20160921.h5")
eos = beq_eos.beta_equilibrium_eos.build_from_stellarcollapse("/home/lroberts/checkout/eosdrivercxx/tables/Hempel_SFHoEOS_rho222_temp180_ye60_version_1.1_20120817.h5", 1.0)
#eos = beq_eos.beta_equilibrium_eos.build_from_stellarcollapse("./LS220_234r_136t_50y_analmu_20091212_SVNr26.h5")

if (gamma > 0.0): 
  eos.write_beta_equilibrium_ye(fname = "gamma" + str(gamma) + "_eos.out") 
else: 
  eos.write_beta_equilibrium_ye(fname = "gamma_base_eos.out") 

tov_sol = tov_solver.tov_solver(eos)
rhocg = np.arange(14, 16, 0.01) 
rhocg = np.power(10, rhocg)

f = open('TOV.out','w')
f.write("# [1] Radius (km) \n")
f.write("# [2] Rho central (g/cc) \n")
f.write("# [3] M_grav (M_sun) \n")
f.write("# [4] M_bar (M_sun) \n")
f.write("# [5] Y_e central \n")
for rhoc in rhocg:
  mod = tov_sol.solve_tov(rhoc, 1.e9)
  mod.get_total_bmass 
  f.write("{0:e} {1:e} {2:e} {3:e} {4:e} \n".format(mod.get_radius()/1.e5, rhoc,
      mod.get_total_gmass(), mod.get_total_bmass(), mod.ye[0]))
