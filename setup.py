#!/usr/bin/env python

from distutils.core import setup

setup(name='tov',
      version='0.1',
      description='Simple code for calculating TOV solutions.',
      author='Luke Roberts',
      author_email='lroberts@tapir.caltech.edu',
      url='http://lukerobertsastro.wordpress.com/',
      packages=['tov'],
     )
