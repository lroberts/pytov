import sys 
import numpy as np  
import matplotlib.pyplot as plt 
import tov.tov_solver as tov_solver   
import tov.beta_equilibrium_eos as beq_eos
import tov.skyrme_eos as skyrme_eos
import os


eos_tables = [ 
#"./BHB_lpEOS_rho234_temp180_ye60_version_1.02_20140422.h5",
#"./Hempel_DD2EOS_rho234_temp180_ye60_version_1.1_20120817.h5"
"./Hempel_SFHoEOS_rho222_temp180_ye60_version_1.1_20120817.h5"
]

for eos_tab in eos_tables:
  eos = beq_eos.beta_equilibrium_eos.build_from_stellarcollapse(eos_tab, 1.0)
  
  base = os.path.splitext(os.path.basename(eos_tab))[0] 
  print base
  
  tov_sol = tov_solver.tov_solver(eos)
  rhocg = np.arange(14, 16, 0.01) 
  rhocg = np.power(10, rhocg)
  
  f = open(base + '.tov','w')
  f.write("# [1] Radius (km) \n")
  f.write("# [2] Rho central (g/cc) \n")
  f.write("# [3] M_grav (M_sun) \n")
  f.write("# [4] M_bar (M_sun) \n")
  f.write("# [5] Y_e central \n")
  for rhoc in rhocg:
    print rhoc
    mod = tov_sol.solve_tov(rhoc, 1.e9)
    mod.get_total_bmass 
    f.write("{0:e} {1:e} {2:e} {3:e} {4:e} \n".format(mod.get_radius()/1.e5, rhoc,
        mod.get_total_gmass(), mod.get_total_bmass(), mod.ye[0]))
